FROM node:16-alpine

WORKDIR /app

COPY package*.json .

RUN npm install

COPY src src

# Start command previously created in package.json
CMD ["npm", "start"]
