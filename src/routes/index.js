const {Router} = require('express')
const router = Router();

router.get('/', (req, res) => res.json({message: "Hello from node"}));

module.exports = router;