# NodeJS dockerize

A NodeJS application that runs in a Docker container. 
This is a practical example for web developers. 
We made a simple application and we will see the step 
by step to both build an image and run the required 
commands to generate the containers